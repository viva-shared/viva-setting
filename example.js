let lib_vset = require('viva-setting')()

let fn = lib_vset.init(__filename) // OR let fn = lib_vset.init('test.ini')
console.log('ini setting in this file - ',fn)
lib_vset.set('temp_section','string_option','abc')
lib_vset.set('temp_section','int_option',42)
lib_vset.set('temp_section','float_option',42.42)
lib_vset.set('temp_section','bool_option',true)
lib_vset.set('temp_section','ip_option','192.168.0.42')

console.log(lib_vset.get('temp_section','string_option','xyz')) // return 'abc'
console.log(lib_vset.getInt('temp_section','int_option',24)) // return 42
console.log(lib_vset.getFloat('temp_section','float_option',24.24)) // return 42.42
console.log(lib_vset.getBool('temp_section','bool_option',false)) // return true
console.log(lib_vset.getIp('temp_section','ip_option','192.168.0.1')) // return '192.168.0.42'

console.log(lib_vset.get('temp_section','missing_string_option','xyz')) //return 'xyz'
console.log(lib_vset.getInt('temp_section','missing_int_option',24)) // return 24
console.log(lib_vset.getFloat('temp_section','missing_float_option',24.24)) // return 24.24
console.log(lib_vset.getBool('temp_section','missing_bool_option',false)) // return false
console.log(lib_vset.getIp('temp_section','missing_ip_option','192.168.0.1')) // return '192.168.0.1'
