// @ts-check
/**
 * @license MIT
 * @author Vitalii vasilev
 */
/** @private */
const lib_path = require('path')
/** @private */
const lib_fs = require('fs')
/** @private */
const lib_os = require('os')
/** @private */
const lib_ini = require('./core.js').ConfigIniParser
/** @private */
const lib_vconv = require('viva-convert')

module.exports = Setting

/**
 * @class (license MIT) library for store app setting in ini file, full example - see example.js
 */
function Setting() {
    if (!(this instanceof Setting)) return new Setting()
}

/** {string} */
Setting.prototype.file = undefined

/**
 * set ini file name
 * @param {string} file app full file name OR ini file name OR ini full file name
 * @returns {string} ini full file name or undefined
 */
Setting.prototype.init = function (file) {
    try {
        let parse = lib_path.parse(file)
        if (parse.ext === '.ini')
        {
            this.file = file
        } else {
            this.file = lib_path.join(parse.dir, parse.name + '.ini')
        }
        if (lib_vconv.isEmpty(parse.dir)) {
            this.file = lib_path.join(__dirname, this.file)
        }
        return this.file
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.init("{0}")',file)
    }
}

/**
 * save setting
 * @param {string} section section in ini file
 * @param {string} option option in ini file
 * @param {string | number | boolean} value value for option
 */
Setting.prototype.set = function (section, option, value) {
    try {
        if (lib_vconv.isEmpty(this.file)) return
        if (lib_vconv.isEmpty(section)) return
        if (lib_vconv.isEmpty(option)) return

        let ini = read(this.file)

        if (!ini.isHaveSection(section)) ini.addSection(section)

        if (lib_vconv.isAbsent(value)) {
            ini.removeOption(section, option)
        } else {
            ini.set(section, option, value)
        }
        write(this.file, ini)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.set("{0}","{1}","{2}")', [section, option, value])
    }
}

/**
 * read setting
 * @param {string} section section in ini file
 * @param {string} option option in ini file
 * @param {string | number | boolean} [default_value] default value for option
 * @returns {string | number | boolean} string or number or boolean or undefined
 */
Setting.prototype.get = function (section, option, default_value) {
    try {
        if (lib_vconv.isEmpty(this.file)) return default_value
        if (lib_vconv.isEmpty(section)) return default_value
        if (lib_vconv.isEmpty(option)) return default_value

        let ini = read(this.file)

        if (!ini.isHaveSection(section) || !ini.isHaveOption(section, option)) {
            this.set (section, option, default_value)
            return default_value
        }

        return ini.get(section, option, default_value)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.get("{0}","{1}","{2}")', [section, option, default_value])
    }
}

/**
 * read setting with type int
 * @param {string} section section in ini file
 * @param {string} option option in ini file
 * @param {number} [default_value] default value for option
 * @returns {number} number or undefined
 */
Setting.prototype.getInt = function (section, option, default_value) {
    try {
        let noconverted = this.get(section, option, default_value)
        return lib_vconv.toInt(noconverted, default_value)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.getInt("{0}","{1}","{2}")', [section, option, default_value])
    }
}

/**
 * read setting with type float
 * @param {string} section section in ini file
 * @param {string} option option in ini file
 * @param {number} [default_value] default value for option
 * @returns {number} number or undefined
 */
Setting.prototype.getFloat = function (section, option, default_value) {
    try {
        let noconverted = this.get(section, option, default_value)
        return lib_vconv.toFloat(noconverted, default_value)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.getFloat("{0}","{1}","{2}")', [section, option, default_value])
    }
}

/**
 * read setting with type boolean
 * @param {string} section section in ini file
 * @param {string} option option in ini file
 * @param {boolean} [default_value] default value for option
 * @returns {boolean} boolean or undefined
 */
Setting.prototype.getBool = function (section, option, default_value) {
    try {
        let noconverted = this.get(section, option, default_value)
        return lib_vconv.toBool(noconverted, default_value)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.getBool("{0}","{1}","{2}")', [section, option, default_value])
    }
}

/**
 * read setting with type ip
 * @param {string} section section in ini file
 * @param {string} option option in ini file
 * @param {string} [default_value] default value for option
 * @returns {string} string or undefined
 */
Setting.prototype.getIp = function (section, option, default_value) {
    try {
        let noconverted = this.get(section, option, default_value)
        return lib_vconv.toIp(noconverted, default_value)
    } catch (error) {
        throw lib_vconv.toErrorMessage(error, 'viva-setting.getIp("{0}","{1}","{2}")', [section, option, default_value])
    }
}

/**
 * @private
 * @param {string} file
 * @param {any} ini
 */
function write(file, ini) {
    lib_fs.writeFileSync(file, ini.stringify(lib_os.EOL))
}

/**
 * @private
 * @param {string} file
 */
function read(file) {
    if (!lib_fs.existsSync(file)) {
        lib_fs.writeFileSync(file, '')
    }
    let ini = new lib_ini(lib_os.EOL)
    ini.parse(lib_fs.readFileSync(file).toString())
    return ini
}