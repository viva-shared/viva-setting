<a name="Setting"></a>

## Setting
(license MIT) library for store app setting in ini file, full example - see example.js

**Kind**: global class  

* [Setting](#Setting)
    * [.file](#Setting+file)
    * [.init(file)](#Setting+init) ⇒ <code>string</code>
    * [.set(section, option, value)](#Setting+set)
    * [.get(section, option, [default_value])](#Setting+get) ⇒ <code>string</code> \| <code>number</code> \| <code>boolean</code>
    * [.getInt(section, option, [default_value])](#Setting+getInt) ⇒ <code>number</code>
    * [.getFloat(section, option, [default_value])](#Setting+getFloat) ⇒ <code>number</code>
    * [.getBool(section, option, [default_value])](#Setting+getBool) ⇒ <code>boolean</code>
    * [.getIp(section, option, [default_value])](#Setting+getIp) ⇒ <code>string</code>

<a name="Setting+file"></a>

### setting.file
{string}

**Kind**: instance property of [<code>Setting</code>](#Setting)  
<a name="Setting+init"></a>

### setting.init(file) ⇒ <code>string</code>
set ini file name

**Kind**: instance method of [<code>Setting</code>](#Setting)  
**Returns**: <code>string</code> - ini full file name or undefined  

| Param | Type | Description |
| --- | --- | --- |
| file | <code>string</code> | app full file name OR ini file name OR ini full file name |

<a name="Setting+set"></a>

### setting.set(section, option, value)
save setting

**Kind**: instance method of [<code>Setting</code>](#Setting)  

| Param | Type | Description |
| --- | --- | --- |
| section | <code>string</code> | section in ini file |
| option | <code>string</code> | option in ini file |
| value | <code>string</code> \| <code>number</code> \| <code>boolean</code> | value for option |

<a name="Setting+get"></a>

### setting.get(section, option, [default_value]) ⇒ <code>string</code> \| <code>number</code> \| <code>boolean</code>
read setting

**Kind**: instance method of [<code>Setting</code>](#Setting)  
**Returns**: <code>string</code> \| <code>number</code> \| <code>boolean</code> - string or number or boolean or undefined  

| Param | Type | Description |
| --- | --- | --- |
| section | <code>string</code> | section in ini file |
| option | <code>string</code> | option in ini file |
| [default_value] | <code>string</code> \| <code>number</code> \| <code>boolean</code> | default value for option |

<a name="Setting+getInt"></a>

### setting.getInt(section, option, [default_value]) ⇒ <code>number</code>
read setting with type int

**Kind**: instance method of [<code>Setting</code>](#Setting)  
**Returns**: <code>number</code> - number or undefined  

| Param | Type | Description |
| --- | --- | --- |
| section | <code>string</code> | section in ini file |
| option | <code>string</code> | option in ini file |
| [default_value] | <code>number</code> | default value for option |

<a name="Setting+getFloat"></a>

### setting.getFloat(section, option, [default_value]) ⇒ <code>number</code>
read setting with type float

**Kind**: instance method of [<code>Setting</code>](#Setting)  
**Returns**: <code>number</code> - number or undefined  

| Param | Type | Description |
| --- | --- | --- |
| section | <code>string</code> | section in ini file |
| option | <code>string</code> | option in ini file |
| [default_value] | <code>number</code> | default value for option |

<a name="Setting+getBool"></a>

### setting.getBool(section, option, [default_value]) ⇒ <code>boolean</code>
read setting with type boolean

**Kind**: instance method of [<code>Setting</code>](#Setting)  
**Returns**: <code>boolean</code> - boolean or undefined  

| Param | Type | Description |
| --- | --- | --- |
| section | <code>string</code> | section in ini file |
| option | <code>string</code> | option in ini file |
| [default_value] | <code>boolean</code> | default value for option |

<a name="Setting+getIp"></a>

### setting.getIp(section, option, [default_value]) ⇒ <code>string</code>
read setting with type ip

**Kind**: instance method of [<code>Setting</code>](#Setting)  
**Returns**: <code>string</code> - string or undefined  

| Param | Type | Description |
| --- | --- | --- |
| section | <code>string</code> | section in ini file |
| option | <code>string</code> | option in ini file |
| [default_value] | <code>string</code> | default value for option |

