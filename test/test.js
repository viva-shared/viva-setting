// @ts-check

// @ts-ignore
const assert = require('assert');
const fs = require('fs')
// @ts-ignore
const testing_lib = require('./../index.js')()

describe("init ini file", function() {
    let fn = testing_lib.init(__filename)
    if (fs.existsSync(fn)) {
        fs.unlinkSync(fn)
    }
    testing_lib.set('temp_section','string_option','abc')
    testing_lib.set('temp_section','string_option2','1:2-3:4')
    testing_lib.set('temp_section','int_option',42)
    testing_lib.set('temp_section','float_option',42.42)
    testing_lib.set('temp_section','bool_option',true)
    testing_lib.set('temp_section','ip_option','192.168.0.42')
    
    it('ini file name should end on "test.ini"', function() {
        assert.equal(fn.substring(fn.length - 8, fn.length), "test.ini")
    })
})

describe("read previously stored value", function() {
    it('by known string option expected previously stored value', function() {
        assert.equal(testing_lib.get('temp_section','string_option','xyz'), "abc")
    })
    it('by known string2 option expected previously stored value', function() {
        assert.equal(testing_lib.get('temp_section','string_option2','xyz'), "1:2-3:4")
    })
    it('by known int option expected previously stored value', function() {
        assert.equal(testing_lib.getInt('temp_section','int_option',24), 42)
    })
    it('by known float option expected previously stored value', function() {
        assert.equal(testing_lib.getFloat('temp_section','float_option',24.24), 42.42)
    })
    it('by known boolean option expected previously stored value', function() {
        assert.equal(testing_lib.getBool('temp_section','bool_option',false), true)
    })
    it('by known ip option expected previously stored value', function() {
        assert.equal(testing_lib.getIp('temp_section','ip_option','192.168.0.1'), '192.168.0.42')
    })
})

describe("read missing value without default", function() {
    it('by unknown string option expected undefined', function() {
        assert.equal(testing_lib.get('temp_section','missing_string_option'), undefined)
    })
    it('by unknown int option expected undefined', function() {
        assert.equal(testing_lib.getInt('temp_section','missing_int_option'), undefined)
    })
    it('by unknown float option expected undefined', function() {
        assert.equal(testing_lib.getFloat('temp_section','missing_float_option'), undefined)
    })
    it('by unknown boolean option expected undefined', function() {
        assert.equal(testing_lib.getBool('temp_section','missing_bool_option'), undefined)
    })
    it('by unknown ip option expected undefined', function() {
        assert.equal(testing_lib.getIp('temp_section','missing_ip_option'), undefined)
    })
})

describe("read missing value with default", function() {
    it('by unknown string option expected default value', function() {
        assert.equal(testing_lib.get('temp_section','missing_string_option','xyz'), "xyz")
    })
    it('by unknown int option expected default value', function() {
        assert.equal(testing_lib.getInt('temp_section','missing_int_option',24), 24)
    })
    it('by unknown float option expected default value', function() {
        assert.equal(testing_lib.getFloat('temp_section','missing_float_option',24.24), 24.24)
    })
    it('by unknown boolean option expected default value', function() {
        assert.equal(testing_lib.getBool('temp_section','missing_bool_option',false), false)
    })
    it('by unknown ip option expected default value', function() {
        assert.equal(testing_lib.getIp('temp_section','missing_ip_option','192.168.0.1'), '192.168.0.1')
    })
})